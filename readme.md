# **README.md onde o trainne irá continuamente responder as perguntas em formas de commit.**


![TerraLab logo](terraLabLogo-Horizontal-300x59.png)

## Indice

* [Atividades teóricas](#AtividadesTeoricas)

* [Atividades Práticas](#QuestoesPraticas)

* [Autor](#Autor)


## Materiais
- [Uma introdução ao Git e Gitflow TerraLAB](http://www2.decom.ufop.br/terralab/uma-introducao-ao-git-e-gitflow/)
- [Terraclub -  Hands on Git](https://drive.google.com/file/d/1_gOa9KG_6nHswQ7xNafjpUQE1i9nLN5B/view)
- [Conventional Commits - Um guia sobre como escrever commits](https://www.conventionalcommits.org/pt-br/v1.0.0/)

**<h2 id="AtividadesTeoricas" align="center"> Questões teóricas </h2>**

1. **O que é Git?**

    **R:** *Git é um controlador de versão de código fonte distribuído, sendo o mais popular e também rápido e eficiente. Ao contrário dos outros controladores de versões de código fonte que se utilizam da diferença entre os arquivos, o git utiliza uma “imagem” do estado desses arquivos.*

2. **O que é a staging area?**

    **R:** *O staging area é o local que se encontram todas as alterações que entrarão no próximo commit. Aqui entram as hashes de arquivos modificados, deletados ou criados.*

3. **O que é o working directory?**

    **R:** *O working directory são todos os arquivos que estão sendo trabalhados no momento.*

4. **O que é um commit?**

    **R:** *É um instantâneo com todas as modificações adicionados no staging area, ao utilizar o commit essas modificações são persistidas no repositório local.*

5. **O que é uma branch?**

    **R:** *É uma ramificação do código, sendo um ponteiro que indica um commit e todos anteriores a ele, sendo a branch principal a master ou main.*

6. **O que é o head no Git?**

    **R:** *O head é um ponteiro que indica uma branch ou um commit específico dentro do repositório.*

7. **O que é um merge?**

    **R:** *Merge é uma função que junta duas branches, adicionando um commit que serve apenas para indicar que aconteceu a junção dessas branches. Ela é essencial para quando se é necessário demonstrar as modificações que estão sendo feitas, por exemplo, quando acontece um pull request.*

8. **Explique os 4 estados de um arquivo no Git**

    **R:** *Os 4 estados de um arquivo no git são:*

 - *Untracked para quando é criado um arquivo (ou adicionado) ou então quando um arquivo é deletado do repositório, ou seja, são aqueles que não estavam no último commit.*

 - *Unmodified é quando não há mudanças neste arquivo desde o último commit.*

 - *Modified é quando houve alguma alteração nesse arquivo desde o último commit.*

 - *Staging é quando ele é adicionado para entrar dentro do próximo commit. Ao realizar o commit esse arquivo volta a ser unmodified.*

9. **Explique o comando git init.**

    **R:** *Inicializa o versionamento de código dentro da pasta em que se encontra. Ela cria um subdiretório com todos os metadados necessários para que haja o versionamento.*

10. **Explique o comando git add.**

    **R:** *Muda o status do arquivo de modified para a staging area, estando assim preparado para o próximo commit.*

11. **Explique o comando git status.**

    **R:** *Mostra como a branch local se encontra em relação a master (main) e os estados de arquivos untracked, modified e staging.*

12. **Explique o comando git commit.**

    **R:** *Coloca os arquivos em uma log dentro do git juntamente com as informações de quem modificou, quando e uma mensagem que serve para descrever o que foi alterado no arquivo. Esta mensagem deve ser utilizado de forma a ser útil indicando exatamente as alterações que foram realizadas.*

13. **Explique o comando git log.**

    **R:** *Git log mostra todas as logs (commits) realizados no repositório com as informações necessárias.*

14. **Explique o comando git checkout -b.**

    **R:** *Cria uma nova branch e já altera a working area para a nova branch.*

15. **Explique o comando git reset e suas três opções.**

    **R:** *As três opções de git reset são:*

 - *soft: que move o HEAD para o commit indicado, mantendo o staging e o work directory inalterados;*
 - *mixed: altera o HEAD para o commit indicado, altera o staging e mantêm o work directory inalterado;*
 - *hard: altera o HEAD para o commit indicado, alterando o staging e o work directory, fazendo assim com que todas alterações depois daquele commit sejam perdidas.*

16. **Explique o comando git revert.**

    **R:** *O git revert cria um novo commit que indica o commit indicado, assim ao contrário do git reset é possível deixar salvo as alterações que já foram feitas, porém colocando um commit que esteja funcionando no topo da pilha de logs.*

17. **Explique o comando git clone.**

    **R:** *O git clone baixa um repositório remoto para a pasta atual.*

18. **Explique o comando git push.**

    **R:** *Envia todas as modificações comitadas do repositório local para o repositório remoto.*

19. **Explique o comando git pull.**

    **R:** *Baixa todas as alterações que se encontram no repositório remoto, como por exemplo branches novas e arquivos modificados.*

20. **Como ignorar o versionamento de arquivos no Git?**

    **R:** *Para ignorar algum arquivo deve-se criar um arquivo .gitignore e listar todos os arquivos que serão ignorados ali. Geralmente utilizado para arquivos que não são necessários para o código, ou então informações de conexão com ferramentas de terceiros (API).*

21. **No terralab utilizamos as branches master ou main, develop e staging. Explique o objetivo de cada uma.**

    **R:** *No gitflow utilizado no terraLab são utilizados a branch master ou main que é o ramo principal e onde se encontra o código da última versão feita, o ramo develop é onde se encontram os arquivos com as modificações que estão sendo feitas pelos desenvolvedores e a branch staging é onde se encontra todos os arquivos prontos para teste que poderão ser incluídos na próxima release, apenas após passar por todas as rotinas de testes que essa branch é mesclada com o ramo principal.*

**<h2 id="QuestoesPraticas" align="center">Questões práticas</h2>**

- [X] 1.  A essa altura, você já deve ter criado a sua conta do GitLab, não é? Crie um repositório público na sua conta, que vai se chamar Atividade Prática e por fim sincronize esse repositório em sua máquina local.

- [X] 2. Dentro do seu reposotorio, crie um arquivo chamado README.md e leia o artigo como fazer um readme.md bonitão e deixe esse README.md abaixo bem bonitão:
README.md onde o trainne irá continuamente responder as perguntas em formas de commit. https://raullesteves.medium.com/github-como-fazer-um-readme-md-bonit%C3%A3o-c85c8f154f8


> :paperclip: Inserção de código, exemplo de commit de *feature*.

- [X] 3.  Crie nesse repositório um arquivo que vai se chamar calculadora.js, abra esse arquivo em seu editor de códigos favoritos e adicione o seguinte código:
node calculadora.js a b

```javascript
const args = process.argv.slice(2);
console.log(parseInt(args[0]) + parseInt(args[1]));
```

    Descubra o que esse código faz através de pesquisas na internet, também descubra como executar um código em javascript e dado que o nome do nosso arquivo é calculadora.js e você entendeu o que o código faz, escreva abaixo como executar esse código em seu terminal:

   **RESPOSTA:** Para executar o código é necessário ter o [Node](https://nodejs.org/en/) instalado na sua máquina, e na hora de executar é preciso utilizar o comando:
   ```
    node calculadora.js n1 n2
   ```
   Sendo n1 e n2 números inteiros e o programa fará a soma desses argumentos que estão sendo passados.
   Isso acontece porque a variável args está "cortando" os dois primeiros argumentos e soma com os argumentos que sobraram args[0] e agrs[1].


- [X] 4.  Agora que você já tem um código feito e a resposta aqui, você precisa subir isso para seu repositório. Sem usar git add . descubra como adicionar apenas um arquivo ao seu histórico de commit e adicione calculadora.js a ele.

    Que tipo de commit esse código deve ter de acordo ao conventional commit.

    **R:** Primeiro deve-se colocar esse arquivo que se encontra *untracked* para o git poder mapea-lo utilizando o "git add calculadora.js" e depois utiliza-se o git commit -m "Feat: calculadora.js".

    Que tipo de commit o seu README.md deve contar de acordo ao conventional commit. Por fim, 
    faça um push desse commit.

    **R:** Aqui podemos já utilizar um atalho fazendo um "git commit -am "docs: README", já que esse arquivo se encontra sendo mapeado pelo git.


- [X] 5.  Copie e cole o código abaixo em sua calculadora.js:

```javascript
const soma = () => {
    console.log(parseInt(args[0]) + parseInt(args[1]));
};

const args = process.argv.slice(2);

soma();
```

  Descubra o que essa mudança representa em relação ao conventional commit e faça o devido commit dessa mudança. 

  **R:** Esse commit cria uma função soma que será executada após o programa capturar os argumentos passados pela linha de comando.

- [X] 6.  João entrou em seu repositório e o deixou da seguinte maneira:

```javascript
const soma = () => {
    console.log(parseInt(args[0]) + parseInt(args[1]));
};

const sub = () => {
    console.log(parseInt(args[0]) - parseInt(args[1]));  
}

const args = process.argv.slice(2);

switch (args[0]) {
    case 'soma':
        soma();
    break;

    case 'sub':
        sub();
    break;

    default:
        console.log('does not support', arg[0]);
}
```

    Depois disso, realizou um git add . 
    e um commit com a mensagem: "Feature: added subtraction"
    faça como ele e descubra como executar o seu novo código.

    Nesse código, temos um pequeno erro, encontre-o e corrija para que a soma e divisão funcionem.n

  **R:** Para funcionar o código era necessário trocar os índices utilizados, pois o indice 0 indica qual função será executada e os indices 1 e 2 são os números a serem computados.

    Por fim, commit sua mudança. 

- [X] 7.  Por causa de joãozinho, você foi obrigado a fazer correções
na sua branch principal! O produto foi pro saco e a empresa perdeu
muito dinheiro porque não conseguiu fazer as suas contas, graças a isso
o seu chefe ficou bem bravo e mandou você dar um jeito disso nunca acontecer.

    Aprenda a criar uma branch, e desenvolva a feature de divisão nessa branch. 

- [] 8.  Agora que a sua divisão está funcionando e você garantiu que
não afetou as outras funções, você está apto a fazer um merge request
Em seu gitlab, descubra como realizá-lo de acordo com o gitflow.

- [] 9.  João quis se redimir dos pecados e fez uma melhoria em seu código,
mas ainda assim, continuou fazendo um push na master, faça a seguinte alteração no código e fez o commit com a mensagem:

    "refactor: calculator abstraction"

```javascript
var x = args[0];
var y = args[2];
var operator = args[1];

function evaluate(param1, param2, operator) {
  return eval(param1 + operator + param2);
}

if ( console.log( evaluate(x, y, operator) ) ) {}
```

    Para piorar a situação, joão não te contou como executar esse novo código, enquanto você não descobre como executá-lo lendo o código, e seu chefe não descobriu que tudo está comprometido, faça um revert através do seu gitlab para que o produto volte ao normal o quanto antes!

- [X] 10. Descubra como executar esse novo código e que operações ele é capaz de realizar. Deixe sua resposta aqui, e explique o que essas funções javascript fazem.

  **R:** Para executar esse código é necessário fazer:
    
    ```
        node calculadora.js <numero> <operator> <numero>
    ```
  
  Onde < numero > indica um valor numérico qualquer e < operador > algum operador da linguagem javascript, podendo ser:
    - "+" para soma (caso coloque uma string esse operador vai concatena-las)
    - "-" para subtração
    - "*" para multiplicação
    - "/" para divisão
    - "%" para modulo (ou seja o resto de uma divisão)
    - "**" para exponenciação
    - "==" ou "===" operador de comparação de igualdade, que retorna true quando igual ou false para diferente
    - "!=" operador de comparação lógico de diferança, retorna true se diferente ou false para igual
    - ">" operador de comparação lógico que retorna se o primeiro número é maior que o segundo
    - "<" operador de comparação lógico que retorna se o primeiro número é menor que o segundo
    - ">=" operador de comparação lógico que retorna se o primeiro número é maior ou igual que o segundo
    - "<=" operador de comparação lógico que retorna se o primeiro número é menor ou igual que o segundo

  No caso das operações multiplicação e exponenciação deve-se colocar os simbolos dentro de aspas para poder funcionar.
  Isto também ocorre com os operadores logicos com exceção do operador de diferença.


**<h2  id="Autor" align="center"> Autor </h2>**

<p align="center">
<img  src="https://gitlab.com/uploads/-/system/user/avatar/8128850/avatar.png?width=400"  width=115><br><sub><strong>Alifer Duarte Silva<strong></sub>
</p>
